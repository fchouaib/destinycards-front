FROM nginx:alpine

COPY img /usr/share/nginx/html/img
COPY js /usr/share/nginx/html/js
COPY css /usr/share/nginx/html/css
COPY index.html /usr/share/nginx/html

